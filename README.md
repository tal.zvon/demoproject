# Django Demo Project

## Description

This is a project demonstrating a Django REST app.
Here, you can see the following elements:

### DevOps Items

- [ERD Diagram](ERD.drawio.svg)
- Docker ([docker-compose.yml](docker-compose.yml), [docker-compose.override.yml](docker-compose.override.yml), [Dockerfiles](conf/dockerfiles/))
- Nginx ([Config](conf/nginx/demo.conf))
- [Python Pip Requirements](conf/requirements/)
- [Git ignore file](.gitignore)
- [GitLab CI/CD](.gitlab-ci.yml)
    - Includes Stages:
        - isort (make sure imports are sorted)
        - black (make sure formatting is consistant across project)
        - type-checking (mypy types)
        - linting (pylint)
        - test (unit and integration tests)
        - build (builds the docker containers, and upload them to GitLab container registry)
- Bash Scripts
    - [Initial Config Script](config.sh) - Run by admin during initial install. Asks for required settings.
    - [Docker Entrypoint Script](website/entrypoint/entrypoint.sh) - Makes sure MySQL is running when Django is starting up before proceeding
    - [Update Script](update.sh) - Pulls the latest docker images from GitLab container registry
- [REST Client Docker Container](supplier_stock_watcher/)
    - Demo of a service that runs in the background, watches for some sort of events, and uses the REST API as a client to update stock

### Django Items

- [Custom Queryset](website/demo_app/models/address.py#L11)
- [Custom Model Manager](website/demo_app/models/address.py#L39)
- [Atomic Queries](website/demo_app/models/order.py#L58)
- [Calculated Fields using Annotation](website/demo_app/models/order.py#L32)
- [Data Migration](website/demo_app/migrations/0002_alter_address_address.py)
- [Reverse Migration](website/demo_app/migrations/0002_alter_address_address.py#L36)
- [Schema Migration](website/demo_app/migrations/0002_alter_supplierinventoryitem_unique_together_and_more.py)
- [Migration to Resolve Merge Conflict](website/demo_app/migrations/0003_merge_20230329_0803.py)
- Custom Middleware ([Definition](website/mysite/middleware.py), [Implementation](website/mysite/settings.py#L92))
- Custom Context Processor ([Definition](website/mysite/context_processors.py), [Implementation](website/mysite/settings.py#L117))
- [Custom Management Command](website/demo_app/management/commands/generatetestdata.py)
- [Foreign Key](website/demo_app/models/order.py#L89)
- [MayToManyField](website/demo_app/models/order.py#L93)
- [REST API View Sets](website/demo_app/views/api/model_views.py) with support for:
    - Filtering (using Django Filters)
    - Search Fields
    - Ordering
- Pagination ([Settings](website/mysite/settings.py#L308-L309), [Custom Class](website/mysite/pagination.py))
- Custom Filter ([Definition](website/accounts/filters.py), [Implementation](website/accounts/views.py#L13))
- [REST Authentication](website/mysite/settings.py#L304-L307)
- [DRF Serializers](website/demo_app/serializers/model.py)
- [Embeds](website/demo_app/views/api/embeds.py)
    - Allows you to hit the supplier list endpoint with a `?embeds=with_product_details` and have an embeded list of products for each supplier, along with details for each product
- [Custom Field on Serializer](website/accounts/serializers.py#L15)
- [Unique Together Constraint](website/demo_app/models/supplier_inventory_item.py#L60)
- [Fixtures](website/fixtures/api_user.json)
    - This fixture contains a hard-coded API Token, and an API user for testing. It would be a bad idea to do this in production, especially if your git repo is public
- Custom Validator ([Definition](website/mysite/validators.py), [Implementation](website/demo_app/models/product.py#L90))
- Django Auth as Decorator ([Definition](website/mysite/lib.py#L6), [Implementation](website/mysite/urls.py#L17))
- Django Auth Bypass ([Definition](website/mysite/lib.py#L26), [Implementation](website/demo_app/urls.py#L38))
- Django Templates ([Layout](website/login_app/templates/login_app/layout.html), [Extending](website/login_app/templates/login_app/login.html))
- Redis Cache ([Setup](website/mysite/settings.py#L312), [Usage](website/demo_app/views/views.py#L10))
- [Class Based Views](website/accounts/views.py#L10)
- [Model Admin Class](website/demo_app/admin/address.py#L24)
- Custom Model Admin Column ([Definition](website/demo_app/admin/address.py#L10), [Implementation](website/demo_app/admin/address.py#L26))
- [Sentry](website/mysite/settings.py#L278)
- [Bulk Updates](website/demo_app/migrations/0002_alter_address_address.py#L10)


### To Do

- Unit Tests
- Integration Tests
- Django Forms
- Celery
- Bulk API Endpoints