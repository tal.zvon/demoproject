#!/bin/bash

######################
# Check for Env File #
######################

# Exit if config.sh hasn't been run yet
if [[ ! -e ".env" ]]
then
    echo "ERROR: Please run config.sh before running this update script" >&2
    exit 1
fi

##################
# Figure out Tag #
##################

if [[ "$#" != 0 ]]
then
    tag="git-commit-$1"
else
    tag="latest"
fi

############################
# Login to GitLab Registry #
############################

echo "===== Authentication ====="
echo ""

docker login https://registry.gitlab.com || { exit 1; }

######################
# Pull Latest Images #
######################

images=(supplier_stock_watcher nginx django)

for image in "${images[@]}"
do
    echo ""
    echo "===== Updating ${image} Image ====="
    echo ""

    docker image pull registry.gitlab.com/tal.zvon/demoproject/${image}:${tag}
done

######################
# Restart Containers #
######################

USE_LS_COMMAND=$(docker-compose ls >/dev/null 2>/dev/null && echo true || echo false)

# Figure out which command to use
if $USE_LS_COMMAND
then
    COMMAND="docker-compose ls -q | grep -q demo && echo true || echo false"
else
    COMMAND="docker-compose ps -q | grep -q '.' && echo true || echo false"
fi

service_running=$(eval "$COMMAND")

# Figure out if we're in DEV or Production mode

ENVIRONMENT=$(cat .env | grep "^ENVIRONMENT[ \t]*=" | cut -d "=" -f 2)

if $service_running
then
    echo ""
    echo "===== Restarting Containers ====="
    echo ""

    docker-compose down

    if [[ "$ENVIRONMENT" == "production" ]]
    then
        echo -e "\nStarting Production services...\n"
        docker-compose -f docker-compose.yml up -d
    else
        echo -e "\nStarting Development services...\n"
        docker-compose up -d
    fi
fi

#######################
# Prune Unused Images #
#######################

echo ""
echo "===== Pruning Docker Images ====="
echo ""
docker image prune -f

######################
# Perform Migrations #
######################

USE_LS_COMMAND=$(docker-compose ls >/dev/null 2>/dev/null && echo true || echo false)

# Figure out which command to use
if $USE_LS_COMMAND
then
    COMMAND="docker-compose ls -q | grep -q demo && echo true || echo false"
else
    COMMAND="docker-compose ps -q | grep -q '.' && echo true || echo false"
fi

service_running=$(eval "$COMMAND")

if $service_running
then
    if [[ "$tag" == "latest" ]]
    then
        # Perform migrations
        echo ""
        echo "===== Migrations ====="
        echo ""
        echo -e "Running migrations...\n"
        docker-compose exec django ./manage.py migrate
    else
        # Don't do migration
        echo ""
        echo "===== Migrations ====="
        echo ""

        echo "WARNING: Skipping migrations because you are NOT on the latest tag!" >&2
        echo "You may need to run migrations manually" >&2
    fi
else
    echo ""
    echo "===== Migrations ====="
    echo ""
    echo "WARNING: You may need to run migrations manually in order to sync your database" >&2
fi
