#!/bin/bash

# Exit if config.sh hasn't been run yet
if [[ ! -e ".env" ]]
then
    echo "ERROR: Please run config.sh before running this install script" >&2
    exit 1
fi

# Exit if we run into problems
set -e

# Print commands as you run them
set -o xtrace

# Check OS version
REDHAT=$(cat /etc/redhat-release | grep -qi "red[ ]*hat" && echo true || echo false)
ROCKY=$(cat /etc/redhat-release | grep -qi rocky && echo true || echo false)
CENTOS=$(cat /etc/redhat-release | grep -qi centos && echo true || echo false)

# Install docker
if [[ ! -e /bin/docker ]]
then
    if [[ "$ROCKY" == "true" || "$CENTOS" == "true" ]]
    then
        echo "Rocky/CentOS detected"

        curl -fsSL https://get.docker.com/ | sh
    elif [[ "$REDHAT" == "true" ]]
    then
        echo "Red Hat detected"

        subscription-manager repos --enable=rhel-7-server-rpms \
                                   --enable=rhel-7-server-extras-rpms \
                                   --enable=rhel-7-server-optional-rpms
        yum -y install yum-utils
        yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
        yum -y install docker-ce
    fi
else
    echo "Docker already installed"
fi

# Install docker-compose
if [[ ! -e /usr/local/bin/docker-compose ]]
then
    # Docker Compose v2
    mkdir -p /usr/local/lib/docker/cli-plugins
    curl -sSL https://github.com/docker/compose/releases/download/v2.6.0/docker-compose-linux-x86_64 -o /usr/local/lib/docker/cli-plugins/docker-compose
    chmod a+x /usr/local/lib/docker/cli-plugins/docker-compose

    # Docker Compose wrapper script
    echo -e '#!/bin/bash\nexec docker compose "$@"' > /usr/local/bin/docker-compose
    chmod a+x /usr/local/bin/docker-compose
else
    echo "docker-compose already installed"
fi

# Start docker and enable it at boot
systemctl start docker
systemctl enable docker

# Login to docker registry
until docker login https://registry.gitlab.com
do
    echo "Invalid username or password - try again" >&2
    sleep 5
done

# Start services
ENVIRONMENT=$(cat .env | grep '^ENVIRONMENT[ \t]*=' | tr -d ' ' | cut -d = -f 2)
if [[ "$ENVIRONMENT" == "production" ]]
then
    echo "Starting production environment"
    docker-compose -f docker-compose.yml up -d
else
    echo "Starting development environment"
    docker-compose up -d
fi

# Wait a few seconds
sleep 15

# Create new user
docker-compose exec django python manage.py createsuperuser
