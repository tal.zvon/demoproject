import os
import random
from decimal import Decimal
from pathlib import Path
from typing import Any, Iterable, Optional, TypeVar, cast

import requests
from cairosvg import svg2png  # type: ignore[import]
from classes import Product, ProductColors, ProductSummary, Supplier, SupplierProduct

# Get API Token
api_token = os.environ.get("API_TOKEN")
assert api_token, "API_TOKEN environment variable not set!"

# URL Prefix
URL_PREFIX = "http://nginx/demo/api/"

# Directory where tool SVGs are stored
image_dir = Path("/tool_images/")

# SVG files
svg_files: list[Path] = []

T = TypeVar("T")


def unwrap(obj: Optional[T]) -> T:
    """
    If an object is of type Optional[T], but you're positive
    it will never be None because you've already checked it,
    you can use this to tell the type checking systems that
    it's safe to assume this will never be None
    """
    assert obj is not None
    return obj


def get_all_suppliers() -> list[Supplier]:
    suppliers = get_all_items("supplier/", embeds=("with_product_details",))

    return [
        Supplier(
            id=supplier['id'],
            product_details=[
                SupplierProduct(
                    supplier_id=supplier['id'],
                    supplier_inventory_item_id=product["supplier_inventory_item_id"],
                    product_id=product["product_id"],
                    quantity=product["quantity"],
                    product_name=product["product_name"],
                    product_color=product["product_color"],
                )
                for product in supplier["product_details"]
            ],
        )
        for supplier in suppliers
    ]


def get_all_items(
    url_suffix: str, embeds: Optional[Iterable[str]] = None
) -> list[dict]:
    """
    Goes through a paginated response, one page at a time, and collects all the items
    Requests the maximum items per page possible, to make this call as efficient as possible
    """
    items: list[dict] = []
    count = -1
    initial_url = URL_PREFIX + url_suffix
    next_url: Optional[str] = None
    params: dict[str, Any] = {"page_size": 1_000_000}

    if embeds:
        params["embeds"] = embeds

    while count == -1 or len(items) != count:
        # Ask for 1,000,000 items per page
        # Django probably won't give us that many, since it sets a maximum, but it will
        # give us the maximum number it will allow, instead of the default
        response = requests.get(
            initial_url if count == -1 else cast(str, next_url),
            headers={"Authorization": f"Token {api_token}"},
            params=params,
        ).json()

        next_url = response["next"]

        if count == -1:
            count = response["count"]

        items.extend(response["results"])

    return items


def generate_random_product_summary() -> ProductSummary:
    """
    Generate a random product name and color
    Since names are based on and tied to the image filename, we pick a random image too
    """
    if not svg_files:
        _read_svg_files()

    product_svg_image_path: Path = random.choice(svg_files)
    product_color: str = random.choice(list(ProductColors)).value
    product_name: str = f"{product_color.title()} {_get_name_from_filename(product_svg_image_path.stem)}"

    return ProductSummary(
        name=product_name,
        color=product_color,
        svg_image_path=product_svg_image_path,
    )


def find_product_for_supplier(
    supplier: Supplier, product: ProductSummary
) -> Optional[SupplierProduct]:
    """
    Given a ProductSummary (a product name and color), see if the supplier's products have that same
    name and color. If so, return it. If not, return None
    """

    for supplier_product in supplier.product_details:
        name_color = (
            supplier_product.product_name,
            supplier_product.product_color,
        )

        if (product.name, product.color) == name_color:
            return supplier_product

    return None


def add_product_stock_to_supplier(product: SupplierProduct, quantity: int) -> None:
    """
    Add stock to this product
    """
    url = URL_PREFIX + f"supplier_inventory_item/{product.supplier_inventory_item_id}/"

    response = requests.patch(
        url,
        data={
            "product_quantity": product.quantity + quantity,
        },
        headers={"Authorization": f"Token {api_token}"},
    )

    response.raise_for_status()


def generate_random_product(product_summary: ProductSummary) -> Product:
    """
    Given a product summary (a product name, color, and image), generate a random product
    with enough details to create a new product
    """
    product_png_image: bytes = svg2png(
        file_obj=product_summary.svg_image_path.open("rb"), output_height=400
    )

    return Product(
        name=product_summary.name,
        color=product_summary.color,
        unit_price=Decimal(random.randrange(10_00, 5000_00)) / 100,
        is_discontinued=False,
        image=(f"{product_summary.svg_image_path.stem}.png", product_png_image),
        test_data=True,
    )


def _ensure_product_exists_in_db(product: Product) -> Product:
    """
    Given a product, make sure it exists in the database
    """
    # Find Product
    products = requests.get(
        URL_PREFIX + "product/",
        params={"name": product.name, "color": product.color},
        headers={"Authorization": f"Token {api_token}"},
    ).json()

    # If the product was found, return it
    if products["results"]:
        assert (
            len(products["results"]) == 1
        ), f"The database has multiple products matching name='{product.name}' color='{product.color}'"

        return Product(**products["results"][0])

    # Otherwise Create Product
    response = requests.post(
        URL_PREFIX + "product/",
        data=dict(
            name=product.name,
            color=product.color,
            unit_price=product.unit_price,
            is_discontinued=product.is_discontinued,
            test_data=product.test_data,
        ),
        files=dict(image=product.image),
        headers={"Authorization": f"Token {api_token}"},
    )

    return Product(**response.json())


def _add_existing_product_to_supplier(
    supplier_id: int, product_id: int, quantity: int
) -> None:
    """
    Given a product id of a product that exists in the database, create a SupplierInventoryItem
    for the given supplier and product with the given quantity
    """

    response = requests.post(
        URL_PREFIX + f"supplier_inventory_item/",
        data={
            "product": product_id,
            "supplier": supplier_id,
            "product_quantity": quantity,
        },
        headers={"Authorization": f"Token {api_token}"},
    )

    response.raise_for_status()


def add_new_product_to_supplier(
    supplier: Supplier, product: Product, quantity: int
) -> None:
    """
    Given a supplied, and a random product that was just generated, adds the product
    to the list of products the supplier carries
    """
    product = _ensure_product_exists_in_db(product)
    _add_existing_product_to_supplier(supplier.id, unwrap(product.id), quantity)


def _read_svg_files() -> None:
    """
    Reads svg files in the tools directory, and adds them to the svg_files global var
    """
    svg_files.extend(
        [
            file
            for file in image_dir.iterdir()
            if file.is_file() and file.suffix == ".svg"
        ]
    )
    svg_files.sort(key=lambda f: f.stem)


def _get_name_from_filename(filename: str) -> str:
    """
    Given a filename as a string, returns a product name from it
    """
    return filename.replace("_", " ").title()
