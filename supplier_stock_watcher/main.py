from random import randint
from typing import Optional

from classes import (
    Product,
    ProductSummary,
    ProductToCreate,
    ProductToUpdate,
    SupplierProduct,
)
from lib import (
    add_new_product_to_supplier,
    add_product_stock_to_supplier,
    find_product_for_supplier,
    generate_random_product,
    generate_random_product_summary,
    get_all_suppliers,
)


def main() -> None:
    # For each supplier, pick N random products that may or may not exist
    # If they exist, increase their stock
    # If the don't, create them with stock
    existing_products_to_update: list[ProductToUpdate] = []
    new_products_to_create: list[ProductToCreate] = []
    for supplier in get_all_suppliers():
        for _ in range(5):
            random_product_summary: ProductSummary = generate_random_product_summary()
            supplier_product: Optional[SupplierProduct] = find_product_for_supplier(
                supplier, random_product_summary
            )
            quantity = randint(1, 10)

            if supplier_product:
                existing_products_to_update.append(
                    ProductToUpdate(supplier_product, quantity)
                )
            else:
                random_product: Product = generate_random_product(
                    random_product_summary
                )
                new_products_to_create.append(
                    ProductToCreate(supplier, random_product, quantity)
                )

    # Add stock for existing products
    # Note: Here, we update the quantity by making one HTTP request per product_to_update
    # This isn't efficient. In a real project, we would either create a new endpoint to do this
    # in bulk with a single request, or add bulk support to the list endpoint
    for product_to_update in existing_products_to_update:
        add_product_stock_to_supplier(
            product_to_update.supplier_product, quantity=product_to_update.quantity
        )

    # Create new products
    # Note: Same here - in a real project, we wouldn't want to do one HTTP request per
    # product_to_create like we're doing here
    for product_to_create in new_products_to_create:
        add_new_product_to_supplier(
            product_to_create.supplier,
            product_to_create.product,
            quantity=product_to_create.quantity,
        )
