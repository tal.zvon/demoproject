#!/usr/bin/env python

###########
# Imports #
###########

import logging
import os
import signal
import socket
import sys
import time
import traceback

import sentry_sdk
from sentry_sdk import capture_exception

####################
# Global Variables #
####################

# If the DEBUG environment variable is set, uses that to set the DEBUG
# global variable
# If the environment variable isn't set, only sets DEBUG to True if we're
# running in a terminal
if "DEBUG" in os.environ:
    # Use Environment Variable
    if os.environ["DEBUG"].lower() == "true":
        DEBUG = True
    elif os.environ["DEBUG"].lower() == "false":
        DEBUG = False
    else:
        raise ValueError("DEBUG environment variable not set to 'true' or 'false'")
else:
    # Use run mode
    if os.isatty(sys.stdin.fileno()):
        DEBUG = True
    else:
        DEBUG = False

# Set Environment
# Controls:
#   Sentry (logs only sent to Sentry in production)
ENVIRONMENT = os.environ.get("ENVIRONMENT", "").lower()

if ENVIRONMENT not in ("development", "production"):
    raise Exception(
        "Please set the 'ENVIRONMENT' environment variable to 'development' or 'production'"
    )

# Script name
script_name = os.path.basename(__file__)

# Get logger
logger = logging.getLogger("main")

# How long to sleep, in seconds, when running in DEBUG mode
DEBUG_SLEEP_TIME = 10

# How long to sleep, in seconds, when running in non-DEBUG mode
PRODUCTION_SLEEP_TIME = 60

################
# Setup Logger #
################

# Define handler and formatter
handler = logging.StreamHandler()
formatter = logging.Formatter("%(name)s %(levelname)s: %(message)s")

# Attach formatter to handler
handler.setFormatter(formatter)

# Setup handler
logger.addHandler(handler)

# Set logging level
if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

########################
# Setup Ctrl+C Handler #
########################


def signal_handler(*_):
    logger.debug("\nExiting...")
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

#############################################
# Check if another instance already running #
#############################################

lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

try:
    lock_socket.bind("\0" + script_name)
except socket.error:
    logger.error(f"Another instance of {script_name} is already running")
    sys.exit(1)

####################
# Configure Sentry #
####################

if ENVIRONMENT == "production" and not DEBUG:
    if "SENTRY_DSN" in os.environ:
        # pylint: disable=abstract-class-instantiated
        sentry_sdk.init(
            # IMPORTANT NOTE: DSN (URL to send events to) must be provided
            # using the SENTRY_DSN env variable
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            # We recommend adjusting this value in production.
            traces_sample_rate=1.0,
            # If you wish to associate users to errors (assuming you are using
            # django.contrib.auth) you may enable sending PII data.
            send_default_pii=False,
            # Do not include body of request that caused the exception
            request_bodies="never",
            # Disable default integrations
            default_integrations=False,
        )
        # pylint: enable=abstract-class-instantiated
    else:
        print("WARNING: SENTRY_DSN environment variable not set - sentry disabled!")

#################
# Other Imports #
#################

# Happens after Sentry is configured, in case something crashes on import

from main import main

########
# Main #
########

if __name__ == "__main__":
    first_run = True

    while True:
        ###########################
        # Wait Between Iterations #
        ###########################

        # If this is the first run, don't wait at all
        # If not, the wait time depends on if we're running in DEBUG mode or not
        # In DEBUG mode, we're troubleshooting, and don't want to wait a long
        # time between iterations

        if first_run:
            first_run = False
        else:
            if DEBUG:
                logger.debug(f"Waiting {DEBUG_SLEEP_TIME} seconds...")
                time.sleep(DEBUG_SLEEP_TIME)
            else:
                time.sleep(PRODUCTION_SLEEP_TIME)

        try:
            # Run main code
            main()
        except Exception as e:
            capture_exception(e)
            logger.error(traceback.format_exc())
