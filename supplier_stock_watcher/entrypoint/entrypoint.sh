#!/bin/bash

# Bash options
set -o errexit
set -o pipefail
set -o nounset

django_ready() {
    python << END
import os
import sys
import requests

api_token = os.environ.get("API_TOKEN")

assert api_token, "API_TOKEN environment variable not set!"

response = requests.get(
    "http://nginx/demo/api/",
    headers={"Authorization": f"Token {api_token}"}
)

try:
    if response.status_code == 200:
        sys.exit(0)
    elif response.status_code == 403:
        print("WARNING: API returned a 403 FORBIDDEN")
        sys.exit(1)
    else:
        sys.exit(1)
except Exception:
    sys.exit(1)

END
}

until django_ready; do
  echo "Waiting for Django to be ready..."
  sleep 5
done
echo "Django is ready"

exec "$@"
