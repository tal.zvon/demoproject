from dataclasses import dataclass
from decimal import Decimal
from enum import Enum
from pathlib import Path
from typing import NamedTuple, Optional


class ProductToUpdate(NamedTuple):
    supplier_product: 'SupplierProduct'
    quantity: int


class ProductToCreate(NamedTuple):
    supplier: 'Supplier'
    product: 'Product'
    quantity: int


@dataclass
class ProductSummary:
    name: str
    color: str
    svg_image_path: Path


@dataclass
class Product:
    """
    A product
    If it has an id, it was retrieved from the database
    If it doesn't, it's one we generated to create
    """

    name: str
    color: str
    unit_price: Decimal
    is_discontinued: bool
    image: tuple[str, bytes]
    test_data: bool
    id: Optional[int] = None


@dataclass
class Supplier:
    id: int
    product_details: list['SupplierProduct']


@dataclass
class SupplierProduct:
    """
    A product description from a supplier's product_details embed
    This represents a specific supplier's product
    """

    supplier_id: int
    supplier_inventory_item_id: int
    product_id: int
    quantity: int
    product_name: str
    product_color: str


class ProductColors(Enum):
    RED = "red"
    ORANGE = "orange"
    YELLOW = "yellow"
    GREEN = "green"
    BLUE = "blue"
    INDIGO = "indigo"
    VIOLET = "violet"
    BLACK = "black"
    WHITE = "white"
