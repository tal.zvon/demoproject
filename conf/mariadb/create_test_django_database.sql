# create databases
CREATE DATABASE IF NOT EXISTS `test_demo`;

# grant rights to demo user for test database
GRANT ALL ON test_.* TO 'demo-user'@'%';
