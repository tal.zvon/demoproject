from django.contrib.auth.views import LoginView
from django.shortcuts import resolve_url


# Login page
class Login(LoginView):
    template_name = "login_app/login.html"
    redirect_authenticated_user = True

    def get_success_url(self):
        # Handles 'next' GET arg
        requested_url = self.get_redirect_url()

        return requested_url or resolve_url("demo_app:main_page")
