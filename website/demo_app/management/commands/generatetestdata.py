import logging
import random
from pathlib import Path
from typing import Optional

from accounts.models import User
from cairosvg import svg2png  # type: ignore[import]
from dateutil import tz
from demo_app.models import (
    Address,
    Order,
    OrderItem,
    Product,
    Supplier,
    SupplierInventoryItem,
)
from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.db.models import Count
from faker import Faker

from .rand import *

###############
# Global Vars #
###############

script_path = Path(__file__).resolve().parent
fake = Faker()
EDMONTON = tz.gettz("America/Edmonton")

#################
# Command Class #
#################


class Command(BaseCommand):
    help = "Generate test data"

    def add_arguments(self, parser):
        parser.add_argument("--addresses", type=int, default=10)
        parser.add_argument("--orders-per-user", type=int, default=3)
        parser.add_argument("--products", type=int, default=10)
        parser.add_argument("--products-per-order", type=int, default=5)
        parser.add_argument("--products-per-supplier", type=int, default=7)
        parser.add_argument("--warehouses-per-supplier", type=int, default=3)
        parser.add_argument("--suppliers", type=int, default=10)
        parser.add_argument("--users", type=int, default=10)
        parser.add_argument("--quiet", action="store_true")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.options = None

        self.logger = logging.getLogger("main")
        self.logger.addHandler(logging.StreamHandler())

    def validate_options(self) -> None:
        if self.options["products_per_order"] > self.options["products"]:
            raise CommandError(
                "You can't have more products per order than there are products"
            )

        if self.options["products_per_supplier"] > self.options["products"]:
            raise CommandError(
                "You can't have more products per supplier than there are products"
            )

    def handle(self, *args, **options):
        # Attach args to instance so methods can use it
        self.options = options

        self.validate_options()

        # Set up logging
        if not self.options["quiet"]:
            self.logger.setLevel(logging.INFO)
        else:
            self.logger.setLevel(logging.WARNING)

        # Generate data
        with transaction.atomic():
            self.generate_users()
            self.generate_products()
            self.generate_suppliers()
            self.generate_addresses()
            self.generate_orders()
            self.generate_order_items()
            self.generate_supplier_inventory_items()

    def generate_users(self) -> None:
        self.logger.info("Generating users...")

        users: list[User] = []
        usernames_in_database: list[str] = list(
            User.objects.values_list("username", flat=True)
        )
        for _ in range(self.options["users"]):
            existing_usernames = [
                user.username for user in users
            ] + usernames_in_database

            while True:
                first_name = fake.first_name()
                last_name = fake.last_name()
                username = f"{first_name[0].lower()}{last_name.lower()}"
                email_domain_name = fake.free_email_domain()
                email = f"{username}@{email_domain_name}"

                if username not in existing_usernames:
                    break

            users.append(
                User(
                    first_name=first_name,
                    last_name=last_name,
                    username=username,
                    email=email,
                    test_data=True,
                )
            )

        User.objects.bulk_create(users)

    def generate_products(self) -> None:
        self.logger.info("Generating products...")

        image_dir = script_path / "tools"

        svg_files: list[Path] = [
            file
            for file in image_dir.iterdir()
            if file.is_file() and file.suffix == ".svg"
        ]
        svg_files.sort(key=lambda f: f.stem)

        possible_combinations = len(svg_files) * len(Product.Color)

        if self.options["products"] > possible_combinations:
            raise CommandError(
                "You requested to generate more products then we know how to generate"
            )

        existing_products = Product.objects.values("name", "color")
        existing_products_as_tuples = [
            (p["name"], p["color"]) for p in existing_products
        ]

        products: list[Product] = []
        products_generated = 0

        # Note: Product.Color can be iterated over without wrapping it in list(),
        # but it makes PyCharm report a typing error with color.value below
        for color in list(Product.Color):
            for svg_file in svg_files:
                if products_generated >= self.options["products"]:
                    break

                product_svg_image: Path = svg_file

                product_name = f"{color.value.title()} {self._get_name_from_filename(product_svg_image.stem)}"
                product_png_image: bytes = svg2png(
                    file_obj=product_svg_image.open("rb"), output_height=400
                )

                if (product_name, color.value) in existing_products_as_tuples:
                    # This combination of product name and color already exists
                    # in the database
                    continue

                products.append(
                    Product(
                        name=product_name,
                        color=color,
                        unit_price=get_random_price(),
                        is_discontinued=fake.boolean(),
                        image=ContentFile(
                            product_png_image, name=f"{product_svg_image.stem}.png"
                        ),
                        test_data=True,
                    )
                )

                products_generated += 1

            if products_generated >= self.options["products"]:
                break

        if self.options["products"] != len(products):
            raise CommandError(
                f"Given the {len(svg_files)} tools you have in your {image_dir} directory, and the "
                f"{len(Product.Color)} colors defined in Product.Color, there are {possible_combinations} combinations "
                "of products we can generate. The database already contains all of those combinations."
            )

        Product.objects.bulk_create(products)

    def generate_suppliers(self) -> None:
        self.logger.info("Generating suppliers...")

        hq_addresses: list[Address] = self.generate_addresses(
            quiet=True, number=self.options["suppliers"]
        )

        warehouse_addresses: list[Address] = self.generate_addresses(
            quiet=True,
            number=self.options["warehouses_per_supplier"] * self.options["suppliers"],
        )

        suppliers: list[Supplier] = []
        for i in range(self.options["suppliers"]):
            contact_first_name = fake.first_name()
            contact_last_name = fake.last_name()

            suppliers.append(
                Supplier(
                    company_name=fake.company(),
                    contact_name=f"{contact_first_name} {contact_last_name}",
                    phone=get_random_phone_number(),
                    email=f"{contact_first_name[0]}{contact_last_name}@{fake.free_email_domain()}".lower(),
                    hq_address=hq_addresses[i],
                    test_data=True,
                )
            )

        Supplier.objects.bulk_create(suppliers)

        # Add warehouse addresses for each supplier
        for i, supplier in enumerate(suppliers):
            supplier.warehouse_addresses.set(
                warehouse_addresses[i : i + self.options["warehouses_per_supplier"]]
            )

    def generate_orders(self) -> None:
        self.logger.info("Generating orders...")

        orders: list[Order] = []
        new_users = User.objects.annotate(order_count=Count('orders')).filter(
            is_staff=False, order_count=0
        )

        for user in new_users:
            for i in range(self.options["orders_per_user"]):
                orders.append(
                    Order(
                        user=user,
                        order_date=fake.date_time_this_year(tzinfo=EDMONTON),
                        test_data=True,
                    )
                )

        Order.objects.bulk_create(orders)

    def generate_order_items(self) -> None:
        self.logger.info("Generating order items...")

        products: list[Product] = list(Product.objects.all())
        order_items: list[OrderItem] = []
        new_orders = Order.objects.annotate(product_count=Count('products')).filter(
            product_count=0
        )

        for order in new_orders:
            products_available_for_order: list[Product] = products[:]
            random.shuffle(products_available_for_order)
            for i in range(self.options["products_per_order"]):
                order_items.append(
                    OrderItem(
                        order=order,
                        product=products_available_for_order.pop(
                            random.randrange(len(products_available_for_order))
                        ),
                        quantity=random.randint(0, 30),
                    )
                )

        OrderItem.objects.bulk_create(order_items)

    def generate_addresses(
        self, quiet: bool = False, number: Optional[int] = None
    ) -> list[Address]:
        if not quiet:
            self.logger.info("Generating addresses...")

        addresses: list[Address] = []
        for _ in range(number or self.options["addresses"]):
            addresses.append(
                Address(
                    apartment=get_random_apartment(),
                    address=fake.street_address(),
                    city=fake.city(),
                    state_or_province=fake.state(),
                    country=fake.country(),
                    zip_or_postal_code=fake.zipcode(),
                    test_data=True,
                )
            )

        return Address.objects.bulk_create(addresses)

    def generate_supplier_inventory_items(self) -> None:
        self.logger.info("Generating supplier inventory items...")

        products: list[Product] = list(Product.objects.all())
        supplier_inventory_items: list[SupplierInventoryItem] = []
        new_suppliers = Supplier.objects.annotate(
            product_count=Count('products')
        ).filter(product_count=0)

        for supplier in new_suppliers:
            products_available_for_supplier: list[Product] = products[:]
            random.shuffle(products_available_for_supplier)
            for i in range(self.options["products_per_supplier"]):
                supplier_inventory_items.append(
                    SupplierInventoryItem(
                        supplier=supplier,
                        product=products_available_for_supplier.pop(
                            random.randrange(len(products_available_for_supplier))
                        ),
                        product_quantity=random.randint(0, 100),
                    )
                )

        SupplierInventoryItem.objects.bulk_create(supplier_inventory_items)

    @staticmethod
    def _get_name_from_filename(filename: str) -> str:
        return filename.replace("_", " ").title()
