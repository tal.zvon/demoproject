import random
from decimal import Decimal
from pathlib import Path

from demo_app.models import Product

###############
# Global Vars #
###############

script_path = Path(__file__).resolve().parent

##########################
# Random Data Generators #
##########################


def get_random_product_color() -> Product.Color:
    return random.choice(list(Product.Color))


def get_random_price() -> Decimal:
    # Between $10.00 and $5000.00
    return Decimal(random.randrange(10_00, 5000_00)) / 100


def get_random_apartment() -> str:
    """
    Might be nothing (blank string), or might be a number as a string
    """
    return random.choice([""] + [str(random.randint(1, 1500))])


def get_random_phone_number() -> str:
    """
    Faker has a phone_number function, but it's too random. It's hard to tell the
    max number of chars it might return, and the format is not consistent. This
    function is more predictable
    """
    random_number = str(random.randint(100_000_0000, 999_999_9999))
    return f"({random_number[:3]}) {random_number[3:6]}-{random_number[6:]}"


__all__ = (
    "get_random_product_color",
    "get_random_price",
    "get_random_apartment",
    "get_random_phone_number",
)
