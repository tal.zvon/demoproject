from accounts.models import User
from demo_app.models import Address, Order, Product, Supplier
from django.core.management.base import BaseCommand

#################
# Command Class #
#################


class Command(BaseCommand):
    help = "Delete test data"

    def handle(self, *args, **options):
        # Note: Some of these will be deleted as part of a CASCADE, like when users are deleted, the associated
        # orders will be deleted, which will also delete associated OrderItems, but it doesn't hurt to not entirely
        # rely on cascade operations and list all the models here so we don't forget any

        # Also Note: OrderItems and SupplierInventoryItems do not have test_data fields, but will be deleted
        # as part of the cascades
        for model in (User, Address, Product, Order, Supplier):
            total_objects_deleted, objects_deleted = model.objects.filter(
                test_data=True
            ).delete()

            for key, value in objects_deleted.items():
                if value == 1:
                    print(f"Deleted one instance of {key}")
                else:
                    print(f"Deleted {value} instances of {key}")
