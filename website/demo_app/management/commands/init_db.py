import sys
from io import StringIO

from django.core.management import call_command
from django.core.management.base import BaseCommand

#################
# Command Class #
#################


class Command(BaseCommand):
    help = "Initialize Database"

    @staticmethod
    def _get_migration_state():
        result = []
        out = StringIO()
        call_command("showmigrations", format="plan", stdout=out)
        out.seek(0)
        for line in out.readlines():
            status, name = line.rsplit(" ", 1)
            result.append((status.strip() == "[X]", name.strip()))
        return result

    @staticmethod
    def _migrate():
        call_command("migrate")

    @staticmethod
    def _any_migrations_performed(state):
        return any([migration[0] for migration in state])

    @staticmethod
    def _all_migrations_performed(state):
        return all([migration[0] for migration in state])

    def handle(self, *args, **options):
        state = self._get_migration_state()

        if not self._any_migrations_performed(state):
            # Perform migration
            self._migrate()
        else:
            # Skip migrations because at least one migration was performed
            # We only do migrations if the database has NEVER had any migrations applied
            if self._all_migrations_performed(state):
                print("All migrations have already been performed")
            else:
                print("WARNING: Some, but NOT ALL migrations have been performed")
                print("You will need to run migrations manually to sync the database!")

            sys.exit(0)
