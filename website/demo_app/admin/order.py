from django.contrib import admin

from ..models import Order

##################
# Custom Columns #
##################


class UserName:
    __name__ = "UserName"
    short_description = "User's Name"

    def __call__(self, instance: Order):
        return f"{instance.user.first_name} {instance.user.last_name}"


class UserUsername:
    __name__ = "UserUsername"
    short_description = "User's Username"

    def __call__(self, instance: Order):
        return instance.user.username


################
# Django Admin #
################


@admin.register(Order)
class OrderModelAdmin(admin.ModelAdmin):
    ordering = ("-order_date",)
    list_display = ("id", UserName(), UserUsername(), "order_date")
    search_fields = ("id", "user__username", "user__first_name", "user__last_name")
    list_filter = ("order_date",)
