from django.contrib import admin

from ..models import Product

################
# Django Admin #
################


@admin.register(Product)
class ProductModelAdmin(admin.ModelAdmin):
    ordering = ("name",)
    list_display = ("id", "name", "color", "unit_price", "is_discontinued")
    search_fields = ("id", "name", "color")
    list_filter = ("color", "is_discontinued")
