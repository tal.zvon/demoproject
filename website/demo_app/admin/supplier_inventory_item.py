from django.contrib import admin

from ..models import SupplierInventoryItem

##################
# Custom Columns #
##################


class SupplierName:
    __name__ = "SupplierName"
    short_description = "Supplier Name"

    def __call__(self, instance: SupplierInventoryItem):
        return instance.supplier.company_name


class ProductName:
    __name__ = "ProductName"
    short_description = "Product Name"

    def __call__(self, instance: SupplierInventoryItem):
        return instance.product.name


################
# Django Admin #
################


@admin.register(SupplierInventoryItem)
class SupplierInventoryItemModelAdmin(admin.ModelAdmin):
    ordering = ("id",)
    list_display = ("id", SupplierName(), ProductName(), "product_quantity")
    search_fields = ("id", "supplier__company_name", "product__name", "product_quantity")
    list_filter = ("product",)
