from .address import AddressModelAdmin
from .order import OrderModelAdmin
from .order_item import OrderItemModelAdmin
from .product import ProductModelAdmin
from .supplier import SupplierModelAdmin
from .supplier_inventory_item import SupplierInventoryItemModelAdmin
