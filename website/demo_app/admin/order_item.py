from django.contrib import admin

from ..models import OrderItem

##################
# Custom Columns #
##################


class UserName:
    __name__ = "UserName"
    short_description = "User's Name"

    def __call__(self, instance: OrderItem):
        return f"{instance.order.user.first_name} {instance.order.user.last_name}"


class UserUsername:
    __name__ = "UserUsername"
    short_description = "User's Username"

    def __call__(self, instance: OrderItem):
        return instance.order.user.username


################
# Django Admin #
################


@admin.register(OrderItem)
class OrderItemModelAdmin(admin.ModelAdmin):
    ordering = ("id",)
    list_display = ("id", UserName(), UserUsername(), "product")
    search_fields = ("id", "order__user__username", "order__user__first_name", "order__user__last_name", "product__name", "quantity")
    list_filter = ("product",)
