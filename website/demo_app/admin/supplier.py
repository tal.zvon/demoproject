from django.contrib import admin

from ..models import Supplier

################
# Django Admin #
################


@admin.register(Supplier)
class SupplierModelAdmin(admin.ModelAdmin):
    ordering = ("company_name",)
    list_display = ("id", "company_name", "phone", "email")
    search_fields = ("id", "company_name", "phone", "email", "contact_name")
