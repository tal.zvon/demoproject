from django.contrib import admin

from ..models import Address

##################
# Custom Columns #
##################


class ZipColumn:
    __name__ = "ZipColumn"
    short_description = "Zip Code"

    def __call__(self, instance: Address):
        return instance.zip_or_postal_code


################
# Django Admin #
################


@admin.register(Address)
class AddressModelAdmin(admin.ModelAdmin):
    ordering = ("country", "state_or_province", "city", "address", "apartment")
    list_display = ("id", "country", "state_or_province", "city", ZipColumn())
    search_fields = (
        "id",
        "country",
        "state_or_province",
        "city",
        "zip_or_postal_code",
        "address",
        "apartment",
    )
    list_filter = ("country", "state_or_province")
