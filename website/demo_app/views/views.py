import time

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import cache_page


# Example of using redis to cache expensive page
@method_decorator(cache_page(15), name="dispatch")
class ExpensiveView(View):
    def get(self, request):
        return self._get_expensive_result()

    @staticmethod
    def _get_expensive_result():
        time.sleep(10)
        return JsonResponse({"result": 42})
