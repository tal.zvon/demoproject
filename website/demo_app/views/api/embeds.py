from collections import OrderedDict, defaultdict
from typing import Callable


class SupplierEmbedsMixin:
    filter_queryset: Callable
    paginate_queryset: Callable
    get_serializer: Callable
    get_queryset: Callable
    get_paginated_response: Callable

    @staticmethod
    def _with_product_details(data: list[OrderedDict]) -> list[OrderedDict]:
        """
        Given a list of suppliers (the data returned from the serializer), figure out the products for each supplier,
        and return the same fields passed to us, but with the additional product_details attribute for each supplier
        that lists the product id, name, color, and quantity
        """
        from demo_app.models import SupplierInventoryItem

        results = data.copy()

        supplier_ids: set[int] = {supplier["id"] for supplier in results}

        supplier_inventory_items = SupplierInventoryItem.objects.filter(
            supplier_id__in=supplier_ids
        ).values(
            "id",
            "supplier_id",
            "product_id",
            "product_quantity",
            "product__name",
            "product__color",
        )

        supplier_inventory_items_by_supplier_id = defaultdict(list)
        for supplier_inventory_item in supplier_inventory_items:
            supplier_inventory_items_by_supplier_id[
                supplier_inventory_item["supplier_id"]
            ].append(
                dict(
                    supplier_inventory_item_id=supplier_inventory_item["id"],
                    product_id=supplier_inventory_item["product_id"],
                    quantity=supplier_inventory_item["product_quantity"],
                    product_name=supplier_inventory_item["product__name"],
                    product_color=supplier_inventory_item["product__color"],
                )
            )

        for supplier in results:
            supplier["product_details"] = supplier_inventory_items_by_supplier_id[
                supplier["id"]
            ]

        return results

    @staticmethod
    def _with_product_names(data: list[OrderedDict]) -> list[OrderedDict]:
        """
        Given a list of suppliers (the data returned from the serializer), figure out the products for each supplier,
        and return the same fields passed to us, but with the additional product_names attribute for each supplier
        that lists the product id, and name
        """
        from demo_app.models import Product

        results = data.copy()

        product_ids: set[int] = {
            product_id for supplier in results for product_id in supplier["products"]
        }

        products = Product.objects.filter(id__in=product_ids).values(
            "id",
            "name",
        )
        products_by_id = {product["id"]: product for product in products}

        for supplier in results:
            supplier["product_names"] = [
                dict(
                    id=products_by_id[product_id]["id"],
                    name=products_by_id[product_id]["name"],
                )
                for product_id in supplier["products"]
            ]

        return results

    @classmethod
    def _add_embeds(
        cls, embeds: list[str], data: list[OrderedDict]
    ) -> list[OrderedDict]:
        """
        Given a list of embeds the client asked to see, figure out which methods to run to add data
        to the response
        """
        # These are the embeds the request is allowed to ask for
        embed_methods = {
            "with_product_details": cls._with_product_details,
            "with_product_names": cls._with_product_names,
        }

        # Make sure all the embeds that were requested were valid
        # Tell the user (frontend dev) if there were invalid embeds requested
        invalid_embeds = set(embeds) - set(embed_methods.keys())
        assert (
            not invalid_embeds
        ), f"The following requested embeds were invalid: {', '.join(invalid_embeds)}"

        for embed, method in embed_methods.items():
            if embed in embeds:
                data = embed_methods[embed](data)

        return data

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        data = serializer.data

        # Look at the query params of the request to figure out
        # which embeds the client asked to see
        embeds = request.query_params.getlist("embeds", [])

        # Add embeds, if requested
        data = self._add_embeds(embeds, list(data))

        return self.get_paginated_response(data)
