from demo_app import models, serializers
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.viewsets import ModelViewSet

from .embeds import SupplierEmbedsMixin


class AddressViewSet(ModelViewSet):
    queryset = models.Address.objects.all()
    serializer_class = serializers.AddressSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filterset_fields = '__all__'
    search_fields = (
        'apartment',
        'address',
        'city',
        'state_or_province',
        'country',
        'zip_or_postal_code',
    )
    ordering_fields = '__all__'
    ordering = ('country', 'state_or_province', 'city', 'address')


class OrderViewSet(ModelViewSet):
    queryset = models.Order.objects.all()
    serializer_class = serializers.OrderSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filterset_fields = (
        'order_date',
        'user__username',
        'user__first_name',
        'user__last_name',
        'products__name',
    )
    search_fields = ('user__first_name', 'user__last_name', 'products__name')
    ordering_fields = '__all__'
    ordering = ('order_date', 'id')


class OrderItemViewSet(ModelViewSet):
    queryset = models.OrderItem.objects.all()
    serializer_class = serializers.OrderItemSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_fields = (
        'product__name',
        'order__order_date',
        'order__user__username',
        'order__user__first_name',
        'order__user__last_name',
    )
    ordering_fields = '__all__'
    ordering = ('id',)


class ProductViewSet(ModelViewSet):
    queryset = models.Product.objects.all()
    serializer_class = serializers.ProductSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filterset_fields = ('name', 'color', 'unit_price', 'is_discontinued')
    search_fields = ('name',)
    ordering_fields = '__all__'
    ordering = ('name',)


class SupplierViewSet(SupplierEmbedsMixin, ModelViewSet):
    queryset = models.Supplier.objects.all()
    serializer_class = serializers.SupplierSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filterset_fields = (
        'company_name',
        'contact_name',
        'phone',
        'email',
        'hq_address__country',
        'hq_address__state_or_province',
        'hq_address__city',
        'hq_address__zip_or_postal_code',
        'warehouse_addresses__country',
        'warehouse_addresses__state_or_province',
        'warehouse_addresses__city',
        'warehouse_addresses__zip_or_postal_code',
    )
    search_fields = ('company_name', 'contact_name', 'phone', 'email')
    ordering_fields = '__all__'
    ordering = ('company_name',)


class SupplierInventoryItemViewSet(ModelViewSet):
    queryset = models.SupplierInventoryItem.objects.all()
    serializer_class = serializers.SupplierInventoryItemSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_fields = (
        'product__name',
        'product_quantity',
        'supplier__company_name',
        'supplier__contact_name',
    )
    ordering_fields = '__all__'
    ordering = ('id',)


__all__ = (
    "AddressViewSet",
    "OrderViewSet",
    "OrderItemViewSet",
    "ProductViewSet",
    "SupplierViewSet",
    "SupplierInventoryItemViewSet",
)
