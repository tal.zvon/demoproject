from decorator_include import decorator_include  # type: ignore[import]
from django.http import HttpResponse
from django.urls import path
from django.views.generic import RedirectView
from mysite.lib import login_not_required
from rest_framework import routers

from . import views

app_name = "demo_app"

##############
# DRF Routes #
##############

router = routers.DefaultRouter()

router.register(r"address", views.AddressViewSet, basename="address")
router.register(r"order", views.OrderViewSet, basename="order")
router.register(r"order_item", views.OrderItemViewSet, basename="order_item")
router.register(r"product", views.ProductViewSet, basename="product")
router.register(r"supplier", views.SupplierViewSet, basename="supplier")
router.register(
    r"supplier_inventory_item",
    views.SupplierInventoryItemViewSet,
    basename="supplier_inventory_item",
)


################
# URL Patterns #
################

urlpatterns = [
    # REST API
    # Note: django rest framework provides its own auth, and doesn't need
    # Django's regular auth
    path("api/", decorator_include(login_not_required, router.urls)),
    # Main Redirect
    path("", RedirectView.as_view(pattern_name="demo_app:hello"), name="main_page"),
    path("hello/", lambda request: HttpResponse("Hello"), name="hello"),
    path("expensive/", views.ExpensiveView.as_view(), name="expensive"),
]
