`0001_initial.py`

- The initial migration with all the models

`0002_alter_address_address.py`

- A commit that fixes a bug someone commited, where the `address` field in
  `demo_app.Address` had `default="123 Fake Street`. It removes the default
  arg, and also makes sure that any existing places in the database where
  this was used are fixed. It does this in a reversible way.

`0002_alter_supplierinventoryitem_unique_together_and_more.py`

- Adds unique constraints to the through tables

## Merge Conflict

Note that both the previous commits are `0002`, and have the same
dependency. This is a conflict, and can be caused by merging two git
branches into `main` that both had migrations.

`0003_merge_20230329_0803.py`

- A commit that resolves the conflict
