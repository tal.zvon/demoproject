from django.db import migrations
from django.db.models import CharField, Value

old_value = Value("123 Fake Street")
new_value = Value("")


def update_address(apps, from_str, to_str):
    Address = apps.get_model("demo_app", "Address")
    Address.objects.filter(address=from_str).update(address=to_str)


def forward_migration(apps, schema_editor):
    update_address(apps, old_value, new_value)


def reverse_migration(apps, schema_editor):
    update_address(apps, new_value, old_value)


class Migration(migrations.Migration):
    dependencies = [
        ('demo_app', '0001_initial'),
    ]

    operations = [
        # Remove default "123 Fake Street" value someone accidentally added
        migrations.AlterField(
            model_name='address',
            name='address',
            field=CharField(max_length=255),
        ),
        # Update existing columns where this was set in the database
        migrations.RunPython(
            forward_migration,
            reverse_migration,
        ),
    ]
