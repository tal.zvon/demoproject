from demo_app import models
from rest_framework.serializers import ModelSerializer


class AddressSerializer(ModelSerializer):
    class Meta:
        model = models.Address
        fields = "__all__"


class OrderSerializer(ModelSerializer):
    class Meta:
        model = models.Order
        fields = "__all__"


class OrderItemSerializer(ModelSerializer):
    class Meta:
        model = models.OrderItem
        fields = "__all__"


class ProductSerializer(ModelSerializer):
    class Meta:
        model = models.Product
        fields = "__all__"


class SupplierSerializer(ModelSerializer):
    class Meta:
        model = models.Supplier
        fields = "__all__"


class SupplierInventoryItemSerializer(ModelSerializer):
    class Meta:
        model = models.SupplierInventoryItem
        fields = "__all__"


__all__ = (
    "AddressSerializer",
    "OrderSerializer",
    "OrderItemSerializer",
    "ProductSerializer",
    "SupplierSerializer",
    "SupplierInventoryItemSerializer",
)
