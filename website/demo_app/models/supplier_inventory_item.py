from django.db.models import CASCADE, ForeignKey, Model, PositiveIntegerField

from .mixins import DjangoShellFields
from .product import Product
from .supplier import Supplier


class SupplierInventoryItem(Model, DjangoShellFields):
    #####################################
    # Variable Declarations for PyCharm #
    #####################################

    # Make PyCharm happy by promising these variables exist
    # before we reference them below

    id: int

    ##########
    # Fields #
    ##########

    product = ForeignKey(
        Product,
        on_delete=CASCADE,
    )
    supplier = ForeignKey(Supplier, on_delete=CASCADE)

    product_quantity = PositiveIntegerField()

    ##########
    # Config #
    ##########

    fields_to_show_in_django_shell = (
        "product_id",
        "supplier_id",
    )

    ################
    # Django Admin #
    ################

    def __str__(self):
        return str(self.id)

    ################
    # Django Shell #
    ################

    def __repr__(self):
        return self.get_django_shell_model_str()

    ########
    # Meta #
    ########

    class Meta:
        verbose_name = "Supplier Inventory Item"
        verbose_name_plural = "Supplier Inventory Items"
        unique_together = ('product', 'supplier')
