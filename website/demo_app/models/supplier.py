from demo_app.managers import DemoManager
from django.core.validators import EmailValidator, MinLengthValidator
from django.db.models import (
    CASCADE,
    BooleanField,
    Case,
    CharField,
    Count,
    ForeignKey,
    ManyToManyField,
    Model,
    QuerySet,
    Value,
    When,
)

from .mixins import DjangoShellFields

############
# QuerySet #
############


class SupplierQuerySet(QuerySet):
    def annotate_is_in_canada(self):
        return self.annotate(
            is_in_canada=Case(
                When(hq_address__country__iexact="canada", then=Value(1)),
                default=Value(0),
                output_field=BooleanField(),
            )
        )

    def annotate_warehouses(self):
        return self.annotate(warehouses=Count("warehouse_addresses"))


#################
# Model Manager #
#################


class SupplierManager(DemoManager["Supplier"]):
    def create_coca_cola_supplier(self, **kwargs) -> "Supplier":
        assert (
            "company_name" not in kwargs
        ), "This method doesn't need a company_name supplied"
        return self.model(company_name="Coca Cola", **kwargs)

    def create_sony_supplier(self, **kwargs) -> "Supplier":
        assert (
            "company_name" not in kwargs
        ), "This method doesn't need a company_name supplied"
        return self.model(company_name="Sony", **kwargs)


_supplier_manager = SupplierManager.from_queryset(SupplierQuerySet)


#########
# Model #
#########


class Supplier(Model, DjangoShellFields):
    ##########
    # Fields #
    ##########

    company_name = CharField(max_length=255, validators=[MinLengthValidator(3)])
    contact_name = CharField(max_length=255, blank=True)

    phone = CharField(max_length=15, blank=True)
    email = CharField(
        max_length=255,
        blank=True,
        validators=[EmailValidator(message="Invalid Email")],
    )

    hq_address = ForeignKey(
        "demo_app.Address", on_delete=CASCADE, related_name="supplier_hq_addresses"
    )

    warehouse_addresses = ManyToManyField(
        "demo_app.Address", related_name="supplier_warehouse_addresses"
    )

    products = ManyToManyField(
        "demo_app.Product",
        through="demo_app.SupplierInventoryItem",
        related_name="suppliers",
    )

    # This gets set to True by generatetestdata.py so that if a production
    # database is ever accidentally littered with test data, it's easy to
    # identify and delete the test data
    test_data = BooleanField(default=False, editable=False)

    #################
    # Model Manager #
    #################

    objects = _supplier_manager()

    ##########
    # Config #
    ##########

    fields_to_show_in_django_shell = ("company_name",)

    ################
    # Django Admin #
    ################

    def __str__(self):
        return self.company_name

    ################
    # Django Shell #
    ################

    def __repr__(self):
        return self.get_django_shell_model_str()

    ########
    # Meta #
    ########

    class Meta:
        verbose_name = "Supplier"
        verbose_name_plural = "Suppliers"
