from django.db.models import CASCADE, ForeignKey, Model, PositiveIntegerField

from .mixins import DjangoShellFields


class OrderItem(Model, DjangoShellFields):
    #####################################
    # Variable Declarations for PyCharm #
    #####################################

    # Make PyCharm happy by promising these variables exist
    # before we reference them below

    id: int

    ##########
    # Fields #
    ##########

    order = ForeignKey("demo_app.Order", on_delete=CASCADE, related_name="order_items")
    product = ForeignKey(
        "demo_app.Product", on_delete=CASCADE, related_name="order_items"
    )

    quantity = PositiveIntegerField()

    ##########
    # Config #
    ##########

    fields_to_show_in_django_shell = ("order_id", "product_id", "quantity")

    ################
    # Django Admin #
    ################

    def __str__(self):
        return str(self.id)

    ################
    # Django Shell #
    ################

    def __repr__(self):
        return self.get_django_shell_model_str()

    ########
    # Meta #
    ########

    class Meta:
        verbose_name = "Order Item"
        verbose_name_plural = "Order Items"
