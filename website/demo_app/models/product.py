from demo_app.managers import DemoManager
from django.core.validators import MinLengthValidator
from django.db.models import (
    BooleanField,
    Case,
    CharField,
    DecimalField,
    ImageField,
    Model,
    QuerySet,
    TextChoices,
    Value,
    When,
)
from django.utils.translation import gettext_lazy as _
from mysite.validators import MimeTypeValidator

from .mixins import DjangoShellFields

############
# QuerySet #
############


class ProductQuerySet(QuerySet):
    def annotate_is_primary_color(self):
        return self.annotate(
            is_primary_color=Case(
                When(color=Product.Color.RED, then=Value(1)),
                When(color=Product.Color.GREEN, then=Value(1)),
                When(color=Product.Color.BLUE, then=Value(1)),
                default=Value(0),
                output_field=BooleanField(),
            )
        )


#################
# Model Manager #
#################


class ProductManager(DemoManager["Product"]):
    def create_red_product(self, **kwargs) -> "Product":
        assert "color" not in kwargs, "This method doesn't need a color supplied"
        return self.model(color=Product.Color.RED, **kwargs)

    def create_green_product(self, **kwargs) -> "Product":
        assert "color" not in kwargs, "This method doesn't need a color supplied"
        return self.model(color=Product.Color.GREEN, **kwargs)


_product_manager = ProductManager.from_queryset(ProductQuerySet)


#########
# Model #
#########


class Product(Model, DjangoShellFields):
    #################
    # Field Choices #
    #################

    class Color(TextChoices):
        RED = "red", _("Red")
        ORANGE = "orange", _("Orange")
        YELLOW = "yellow", _("Yellow")
        GREEN = "green", _("Green")
        BLUE = "blue", _("Blue")
        INDIGO = "indigo", _("Indigo")
        VIOLET = "violet", _("Violet")
        BLACK = "black", _("Black")
        WHITE = "white", _("White")

    ##########
    # Fields #
    ##########

    name = CharField(max_length=255, validators=[MinLengthValidator(3)])

    # In a real app, this would not use choices - there are far too many possible
    # colors to list, and some products can be multicolored, but this is an example of using choices
    color = CharField(max_length=6, choices=Color.choices)
    unit_price = DecimalField(max_digits=15, decimal_places=2)
    is_discontinued = BooleanField(default=False)
    image = ImageField(
        upload_to="products/images/%Y/%m/%d/",
        validators=[MimeTypeValidator("image/png")],
        null=True,
    )

    # This gets set to True by generatetestdata.py so that if a production
    # database is ever accidentally littered with test data, it's easy to
    # identify and delete the test data
    test_data = BooleanField(default=False, editable=False)

    #################
    # Model Manager #
    #################

    objects = _product_manager()

    ##########
    # Config #
    ##########

    fields_to_show_in_django_shell = ("name", "color")

    ################
    # Django Admin #
    ################

    def __str__(self):
        return self.name

    ################
    # Django Shell #
    ################

    def __repr__(self):
        return self.get_django_shell_model_str()

    ########
    # Meta #
    ########

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
