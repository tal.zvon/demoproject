from datetime import datetime
from decimal import Decimal
from typing import TYPE_CHECKING, Optional

from demo_app.managers import DemoManager
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import (
    CASCADE,
    BooleanField,
    DateTimeField,
    F,
    ForeignKey,
    ManyToManyField,
    Model,
    QuerySet,
    Sum,
)
from django.utils import timezone

from .mixins import DjangoShellFields

if TYPE_CHECKING:
    from accounts.models import User

############
# QuerySet #
############


class OrderQuerySet(QuerySet):
    def annotate_total_amounts(self) -> "OrderQuerySet":
        return self.annotate(
            total=Sum(
                F("order_items__quantity") * F("order_items__product__unit_price")
            )
        )


#################
# Model Manager #
#################


class OrderManager(DemoManager["Order"]):
    @staticmethod
    def create_single_product_order(
        user: "User",
        product_id: int,
        quantity: int,
        order_date: Optional[datetime] = None,
    ) -> 'Order':
        from .order_item import OrderItem

        # Default to now if no order_date given
        order_date = order_date or timezone.now()

        with transaction.atomic():
            new_order = Order.objects.create(user=user, order_date=order_date)
            OrderItem.objects.create(
                order=new_order, product_id=product_id, quantity=quantity
            )

        return new_order


_order_manager = OrderManager.from_queryset(OrderQuerySet)

#########
# Model #
#########


class Order(Model, DjangoShellFields):
    #####################################
    # Variable Declarations for PyCharm #
    #####################################

    # Make PyCharm happy by promising these variables exist
    # before we reference them below

    id: int
    user_id: int

    ##########
    # Fields #
    ##########

    user = ForeignKey(get_user_model(), on_delete=CASCADE, related_name="orders")

    order_date = DateTimeField(default=timezone.now)

    products = ManyToManyField(
        "demo_app.Product",
        through="demo_app.OrderItem",
        related_name="orders",
    )

    # This gets set to True by generatetestdata.py so that if a production
    # database is ever accidentally littered with test data, it's easy to
    # identify and delete the test data
    test_data = BooleanField(default=False, editable=False)

    #################
    # Model Manager #
    #################

    objects = _order_manager()

    ##########
    # Config #
    ##########

    fields_to_show_in_django_shell = ("user_id",)

    ################
    # Django Admin #
    ################

    def __str__(self):
        return str(self.user_id)

    ################
    # Django Shell #
    ################

    def __repr__(self):
        return self.get_django_shell_model_str()

    ######################
    # Additional Methods #
    ######################

    # These methods get added to an individual order, rather than a QuerySet[Order]

    def total_amount(self) -> Decimal:
        from .order_item import OrderItem

        return OrderItem.objects.filter(order_id=self.id).aggregate(
            total=Sum(F("quantity") * F("product__unit_price"))
        )["total"]

    ########
    # Meta #
    ########

    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"
