from demo_app.managers import DemoManager
from django.db.models import BooleanField, Case, CharField, Model, QuerySet, Value, When

from .mixins import DjangoShellFields

############
# QuerySet #
############


class AddressQuerySet(QuerySet):
    def annotate_in_canada(self):
        return self.annotate(
            in_canada=Case(
                When(country__iexact="canada", then=Value(1)),
                default=Value(0),
                output_field=BooleanField(),
            ),
        )

    def annotate_in_america(self):
        return self.annotate(
            in_america=Case(
                When(country__iexact="usa", then=Value(1)),
                When(country__iexact="us", then=Value(1)),
                When(country__iexact="america", then=Value(1)),
                When(country__icontains="united states", then=Value(1)),
                default=Value(0),
                output_field=BooleanField(),
            ),
        )


#################
# Model Manager #
#################


class AddressManager(DemoManager["Address"]):
    def create_canadian_address(self, **kwargs) -> "Address":
        assert "country" not in kwargs, "This method doesn't need a country supplied"
        return self.model(country="Canada", **kwargs)

    def create_american_address(self, **kwargs) -> "Address":
        assert "country" not in kwargs, "This method doesn't need a country supplied"
        return self.model(country="United States", **kwargs)


_address_manager = AddressManager.from_queryset(AddressQuerySet)


#########
# Model #
#########


class Address(Model, DjangoShellFields):
    ##########
    # Fields #
    ##########

    apartment = CharField(max_length=255, blank=True)
    address = CharField(max_length=255)
    city = CharField(max_length=255)
    state_or_province = CharField(max_length=255)
    country = CharField(max_length=255)
    zip_or_postal_code = CharField(max_length=255)

    # This gets set to True by generatetestdata.py so that if a production
    # database is ever accidentally littered with test data, it's easy to
    # identify and delete the test data
    test_data = BooleanField(default=False, editable=False)

    #################
    # Model Manager #
    #################

    objects = _address_manager()

    ##########
    # Config #
    ##########

    fields_to_show_in_django_shell = (
        "apartment",
        "address",
        "city",
        "state_or_province",
        "country",
        "zip_or_postal_code",
    )

    ################
    # Django Admin #
    ################

    def __str__(self):
        return self.address

    ################
    # Django Shell #
    ################

    def __repr__(self):
        return self.get_django_shell_model_str()

    ########
    # Meta #
    ########

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"
