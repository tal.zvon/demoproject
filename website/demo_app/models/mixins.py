from itertools import chain
from typing import Tuple


class DjangoShellFields:
    # Override this in the model
    fields_to_show_in_django_shell: Tuple[str, ...] = ()

    # Only override this in the model if you don't want the id field included
    # in Django Shell
    extra_fields_to_show_in_django_shell: Tuple[str, ...] = ("id",)

    def get_django_shell_model_str(self):
        model_name = self.__class__.__name__
        return_strings = []

        for field in chain(
            self.extra_fields_to_show_in_django_shell,
            self.fields_to_show_in_django_shell,
        ):
            value = getattr(self, field)

            # Models that aren't saved don't have an ID (have id=None)
            # We still want to see it
            if field == "id" or value:
                return_strings.append(f"{field}={value!r}")

        return f"{model_name}({', '.join(return_strings)})"
