from django.db.models import Manager


class DemoManager(Manager):
    def delete_test_data(self):
        return self.filter(test_data=True).delete()
