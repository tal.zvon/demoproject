from django.contrib import admin
from django.contrib.auth.admin import Group  # type: ignore[attr-defined]
from django.contrib.auth.admin import UserAdmin

from .models import User


class MyUserAdmin(UserAdmin):
    model = User

    # Fields to display when listing all users
    list_display = ("username", "email", "first_name", "last_name")

    # Define User Add Form - form admin sees when adding a user
    add_fieldsets = (
        (None, {"fields": ("username", "password1", "password2")}),
        ("Personal info", {"fields": ("first_name", "last_name", "email")}),
        ("Permissions", {"fields": ("is_active", "is_staff", "is_superuser")}),
    )

    # Defined User Edit Form - form admin sees when editing a user
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        ("Personal info", {"fields": ("first_name", "last_name", "email")}),
        ("Permissions", {"fields": ("is_active", "is_staff", "is_superuser")}),
    )

    # Hide API user from Django admin, so staff doesn't accidentally delete
    # it. This is the user that supplier_stock_watcher uses to connect to Django
    def get_queryset(self, request):
        return super().get_queryset(request).exclude(username="api")


# Register ModelAdmins
admin.site.register(User, MyUserAdmin)

# Hide the builtin Group from admin portal
# Groups are the same thing as Companies for us - it would be redundant
# to show them twice
admin.site.unregister(Group)
