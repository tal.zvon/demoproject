from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import AccountManager


class User(AbstractUser):
    # Note: These fields are present in AbstractUser, but we're overriding some of their parameters

    first_name = models.CharField(_("first name"), max_length=150)
    last_name = models.CharField(_("last name"), max_length=150)
    email = models.EmailField(_("email address"), blank=True, unique=True)
    address = models.ForeignKey(
        "demo_app.Address",
        on_delete=models.CASCADE,
        null=True,
    )

    # This gets set to True by generatetestdata.py so that if a production
    # database is ever accidentally littered with test data, it's easy to
    # identify and delete the test data
    test_data = models.BooleanField(default=False, editable=False)

    objects = AccountManager()

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"
