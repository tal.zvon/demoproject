from django.contrib.auth.models import AbstractUser, UserManager


class AccountManager(UserManager[AbstractUser]):
    def delete_test_data(self):
        return self.filter(test_data=True).delete()
