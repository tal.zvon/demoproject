from decorator_include import decorator_include  # type: ignore[import]
from django.urls import path
from mysite.lib import login_not_required
from rest_framework import routers

from . import views

app_name = "accounts"

##############
# DRF Routes #
##############

router = routers.DefaultRouter()

router.register(r"user", views.UserViewSet, basename="user")

################
# URL Patterns #
################

urlpatterns = [
    # REST API
    # Note: django rest framework provides its own auth, and doesn't need
    # Django's regular auth
    path("api/", decorator_include(login_not_required, router.urls)),
]
