from accounts.models import User
from django.db.models.query import QuerySet
from django_filters.rest_framework import FilterSet, filters


class UserFilter(FilterSet):
    has_more_than_x_orders = filters.NumberFilter(field_name="orders", lookup_expr="gt")
    # OR
    has_more_than_n_orders = filters.NumberFilter(
        method="has_more_than_n_orders_filter"
    )

    @staticmethod
    def has_more_than_n_orders_filter(
        queryset: QuerySet[User], key: str, value: int
    ) -> QuerySet[User]:
        return queryset.filter(orders__gt=value)

    class Meta:
        model = User
        fields = {
            'id': ('exact',),
            'username': ('exact', 'icontains'),
            'last_login': ('exact', 'year__gt'),
            'first_name': ('exact', 'icontains'),
            'last_name': ('exact', 'icontains'),
            'email': ('exact', 'icontains'),
            'is_staff': ('exact',),
            'is_active': ('exact',),
            'date_joined': ('exact', 'year__gt'),
        }
