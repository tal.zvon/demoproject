from typing import TYPE_CHECKING

from accounts import models
from django.utils import timezone
from rest_framework.serializers import ModelSerializer, SerializerMethodField

if TYPE_CHECKING:
    from .models import User


class UserSerializer(ModelSerializer):
    user_for = SerializerMethodField()

    @staticmethod
    def get_user_for(obj: 'User'):
        time_since_joined = timezone.now() - obj.date_joined
        days_since_joined = time_since_joined.days
        day_str = "day" if days_since_joined == 1 else "days"
        return f"{days_since_joined} {day_str}"

    class Meta:
        model = models.User
        exclude = ('password',)
