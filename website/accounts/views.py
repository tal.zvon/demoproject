from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.viewsets import ModelViewSet

from .filters import UserFilter
from .models import User
from .serializers import UserSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filterset_class = UserFilter
    serializer_class = UserSerializer
    search_fields = ('username', 'email', 'first_name', 'last_name')
    ordering_fields = '__all__'
    ordering = ('last_name', 'first_name')
