#!/bin/bash

DEBUG=$(echo "$DEBUG" | tr '[:upper:]' '[:lower:]')

if [[ "$DEBUG" == "true" ]]
then
    python manage.py runserver 0.0.0.0:8000
else
    gunicorn --worker-class eventlet -w 3 --bind unix:/sockets/demo.sock mysite.wsgi
fi
