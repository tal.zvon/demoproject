#!/bin/bash

# Bash options
set -o errexit
set -o pipefail
set -o nounset

# Define mysql_ready function
mysql_ready() {
    python << END
import sys
import MySQLdb
import configparser
from MySQLdb._exceptions import OperationalError

# Read django database config file
config = configparser.ConfigParser()
config.read("/code/db.cnf")

try:
    db=MySQLdb.connect(
        host=config.get("client", "host"),
        user=config.get("client", "user"),
        passwd=config.get("client", "password"),
        db=config.get("client", "database"),
    )

    db.ping()
except OperationalError:
    sys.exit(1)

END
}

until mysql_ready; do
  echo "Waiting for MySQL to become available..."
  sleep 5
done

echo "MySQL is available"

if [[ "${DEBUG:-false}" == "true" ]]
then
    # In DEBUG mode, remove the static dir so nginx can send requests for
    # static files directly to django's dev server
    rm -rf /code/.static/*
else
    # Collect the static files for Nginx
    python manage.py collectstatic --noinput --clear
fi

# init_db will only perform the migrations if they've never been done before
# If ANY migrations have been performed, it will NOT perform migrations
# This means we default to NOT automatically performing migrations, but do
# if the database is empty
python manage.py init_db

# Important: This will override the api user, and token every time
# If nothing in the fixture has changed, that's not a problem
# As soon as someone updates api_user.json, on next django run, it will
# update it
# This is intentional, making it easy to update the token if it's ever
# compromised
python manage.py loaddata fixtures/api_user.json

exec "$@"
