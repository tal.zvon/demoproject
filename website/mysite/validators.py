from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible


def _read_file(file, length=None):
    if length is None:
        data = file.read()
    else:
        data = file.read(length)
    file.seek(0)
    return data


@deconstructible
class MimeTypeValidator:
    def __init__(self, expected_mime_type):
        self.expected_mime_type = expected_mime_type

    def __call__(self, file):
        import magic

        mime = magic.Magic(mime=True)
        file_type = mime.from_buffer(_read_file(file, length=1024))

        if file_type != self.expected_mime_type:
            raise ValidationError(
                f"This file does not appear to be a '{self.expected_mime_type}' file"
            )

    def __eq__(self, other):
        return self.expected_mime_type == other.expected_mime_type
