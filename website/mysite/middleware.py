from django.contrib.auth import logout
from django.http import HttpResponseForbidden


# Example of middleware
class BanAllJohns:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated and request.user.first_name.lower() == "john":
            logout(request)
            return HttpResponseForbidden("Get out of here John!")
        return self.get_response(request)
