from decorator_include import decorator_include  # type: ignore[import]
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.http import HttpResponseRedirect
from django.urls import include, path, reverse_lazy

from .lib import login_required

urlpatterns = [
    # Admin Apps
    path("admin/", admin.site.urls),
    path("auth/", include("login_app.urls")),
    path("", lambda r: HttpResponseRedirect(reverse_lazy("login_app:login"))),
    # User Apps
    path("demo/", decorator_include(login_required, ("demo_app.urls", "demo_app"))),
    # Accounts
    path("accounts/", decorator_include(login_required, ("accounts.urls", "accounts"))),
    # Favicon
    path(
        "favicon.ico",
        lambda r: HttpResponseRedirect(
            staticfiles_storage.url("global/icons/demo_favicon.ico")
        ),
    ),
    # Debug Toolbar
    path("__debug__/", include("debug_toolbar.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Change title on admin login page
admin.site.site_header = settings.ADMIN_SITE_HEADER
