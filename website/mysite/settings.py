import os
from pathlib import Path
from typing import List

import sentry_sdk
from django.urls import reverse_lazy
from sentry_sdk.integrations.django import DjangoIntegration

####################
# General Settings #
####################

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
if os.environ.get("DEBUG", "false").lower() == "true":
    DEBUG = True
else:
    DEBUG = False

# Set Environment
# Controls:
#   Watermark (only appears in dev environment)
#   Sentry (logs only sent to Sentry in production)
ENVIRONMENT = os.environ.get("ENVIRONMENT", "").lower()

if ENVIRONMENT not in ("development", "production"):
    raise Exception(
        "Please set the 'ENVIRONMENT' environment variable to 'development' or 'production'"
    )

REDIS_KEY = os.environ['REDIS_KEY']

################
# Safety Check #
################

# Make sure production never runs with DEBUG=True
assert not (
    ENVIRONMENT == "production" and DEBUG
), "You should never run DEBUG=True in production environment!"

#################
# ALLOWED HOSTS #
#################

# If the server was hosted, this would be the domain name
ALLOWED_HOSTS: List[str] = ["*"]

##########################
# Installed Applications #
##########################

INSTALLED_APPS = [
    # My Apps
    'accounts.apps.AccountsConfig',
    'login_app.apps.LoginAppConfig',
    'demo_app.apps.DemoAppConfig',
    # Django Apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Third Party Apps
    'debug_toolbar',
    'crispy_forms',
    'crispy_bootstrap4',
    'rest_framework',
    'rest_framework.authtoken',
    'django_filters',
]

##############
# Middleware #
##############

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'mysite.middleware.BanAllJohns',
]

###########
# URLCONF #
###########

ROOT_URLCONF = 'mysite.urls'

#############
# Templates #
#############

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # Add "settings" context variable to all templates
                "mysite.context_processors.settings",
            ],
        },
    },
]

############
# Fixtures #
############

# Tells unit tests where to find fixtures
FIXTURE_DIRS = [BASE_DIR / "fixtures"]

###############
# Application #
###############

WSGI_APPLICATION = 'mysite.wsgi.application'

############
# Database #
############

if os.environ.get("DB_TYPE", "").lower() == "sqlite":
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": BASE_DIR / "db.sqlite3",
        }
    }
elif os.environ.get("DB_TYPE", "").lower() == "mysql":
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.mysql",
            "OPTIONS": {
                "read_default_file": str(BASE_DIR / "db.cnf"),
                "init_command": "SET sql_mode='STRICT_TRANS_TABLES'",
            },
        }
    }
else:
    raise Exception(
        "Please set the 'DB_TYPE' environment variable to 'sqlite' or 'mysql'"
    )


#######################
# Password Validation #
#######################

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


########################
# Internationalization #
########################

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Edmonton'

USE_I18N = True

USE_TZ = True

############
# Defaults #
############

# Default primary key field type
# https://docs.djangoproject.com/en/4.1/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

##########################################
# Static Files (CSS, JavaScript, Images) #
##########################################

# The URL for all static files
STATIC_URL = "/static/"

# The dir where collectstatic will put all static files
STATIC_ROOT = BASE_DIR / ".static/"

# Additional dirs to search for static files
# 'static' dir in all apps will already be searched
STATICFILES_DIRS = [BASE_DIR / "static"]

##############
# User Model #
##############

AUTH_USER_MODEL = "accounts.User"

################
# Crispy Forms #
################

CRISPY_TEMPLATE_PACK = "bootstrap4"

###############
# Media Files #
###############

MEDIA_ROOT = BASE_DIR / "media/"
MEDIA_URL = "/media/"

######################
# Login App Settings #
######################

# URL to load when logging out
LOGOUT_REDIRECT_URL = reverse_lazy("login_app:login")

# URL to load when unauthenticated user tries to load a page that
# requires authentication to view
LOGIN_URL = reverse_lazy("login_app:login")

###################
# X Frame Options #
###################

# Allow PDFs to be embedded on our site, as long as their source is on our
# site
X_FRAME_OPTIONS = "SAMEORIGIN"

#####################
# Admin Login Title #
#####################

# This will show up on /admin/login
ADMIN_SITE_HEADER = "Demo Admin Login"

#################
# Debug Toolbar #
#################

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": lambda request: ENVIRONMENT == "development" and DEBUG,
}

##########
# Sentry #
##########

if ENVIRONMENT == "production" and not DEBUG:
    if "SENTRY_DSN" in os.environ:
        # pylint: disable=abstract-class-instantiated
        sentry_sdk.init(
            # IMPORTANT NOTE: DSN (URL to send events to) must be provided
            # using the SENTRY_DSN env variable
            integrations=[DjangoIntegration()],
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            # We recommend adjusting this value in production.
            traces_sample_rate=1.0,
            # If you wish to associate users to errors (assuming you are using
            # django.contrib.auth) you may enable sending PII data.
            send_default_pii=False,
            # Do not include body of request that caused the exception
            request_bodies="never",
            # Disable default integrations
            default_integrations=False,
        )
        # pylint: enable=abstract-class-instantiated
    else:
        print("WARNING: SENTRY_DSN environment variable not set - sentry disabled!")

##################
# Rest Framework #
##################

REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ],
    "DEFAULT_PAGINATION_CLASS": "mysite.pagination.DemoPaginationClass",
    "PAGE_SIZE": 100,
}

###############
# REDIS CACHE #
###############

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.redis.RedisCache",
        "LOCATION": f"redis://default:{REDIS_KEY}@redis:6379",
    }
}
