from rest_framework.pagination import PageNumberPagination


class DemoPaginationClass(PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 500
