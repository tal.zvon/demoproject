#!/bin/bash

#############
# Pre-Setup #
#############

if [[ ! -e .env ]]
then
    touch .env
fi

###############
# Environment #
###############

clear
echo "Please select the environment you are currently in"
echo ""
echo "Production Environment:"
echo ""
echo "    The production environment is the real environment we'll be running"
echo "    the server on. There should only ever be one production environment"
echo ""
echo "Development Environment:"
echo ""
echo "    The development environment is any environment that will NOT have real data."
echo ""
echo "1. Production"
echo "2. Development"
echo ""

while true
do
    read -r -p "Your answer: " ENVIRONMENT

    case $ENVIRONMENT in
        1)
            if grep -q "ENVIRONMENT=" .env
            then
                # Update existing entry
                sed -i 's/^ENVIRONMENT=.*/ENVIRONMENT=production/g' .env
            else
                # Add new entry
                echo "ENVIRONMENT=production" >> .env
            fi

            echo -e "\nEnvironment set to Production"

            break
            ;;
        2)
            if grep -q "ENVIRONMENT=" .env
            then
                # Update existing entry
                sed -i 's/^ENVIRONMENT=.*/ENVIRONMENT=development/g' .env
            else
                # Add new entry
                echo "ENVIRONMENT=development" >> .env
            fi

            echo -e "\nEnvironment set to Development"

            break
            ;;
        *)
            echo -e "\nPlease select 1, 2, or Ctrl+C to exit"
            ;;
    esac
done

read -r -p "Press enter to continue"

###################
# Sentry - Django #
###################

clear

CURRENT_VALUE=$(grep "DJANGO_SENTRY_DSN=" .env | cut -d '=' -f 2)

echo "Please enter the Sentry DSN for Django"
echo ""
echo "The DSN is the URL that lets us know where to send the Sentry error reports"
echo "You can find in your project settings on sentry.io under 'Client Keys (DSN)'"
echo ""
echo "Leave blank to disable Sentry"
echo ""

if [[ -z "$CURRENT_VALUE" ]]
then
    read -r -p "Django DSN: " DJANGO_SENTRY_DSN
else
    read -r -e -p "Django DSN: " -i "$CURRENT_VALUE" DJANGO_SENTRY_DSN
fi

DJANGO_SENTRY_DSN=$(echo "$DJANGO_SENTRY_DSN" | tr -d ' ' | tr -d '\n')

if [[ -z "$DJANGO_SENTRY_DSN" ]]
then
    echo "Sentry disabled for Django"
else
    if grep -q "DJANGO_SENTRY_DSN=" .env
    then
        # Update existing entry
        # Source: https://stackoverflow.com/a/2705678/6423456
        ESCAPED_DJANGO_SENTRY_DSN=$(printf '%s\n' "$DJANGO_SENTRY_DSN" | sed -e 's/[\/&]/\\&/g')
        sed -i "s/^DJANGO_SENTRY_DSN=.*/DJANGO_SENTRY_DSN=${ESCAPED_DJANGO_SENTRY_DSN}/g" .env
    else
        # Add new entry
        echo "DJANGO_SENTRY_DSN=$DJANGO_SENTRY_DSN" >> .env
    fi

    echo -e "\nDjango Sentry DSN configured"
fi

read -r -p "Press enter to continue"

###################################
# Sentry - Supplier Stock Watcher #
###################################

clear

CURRENT_VALUE=$(grep "SSW_SENTRY_DSN=" .env | cut -d '=' -f 2)

echo "Please enter the Sentry DSN for Supplier Stock Watcher"
echo ""
echo "The DSN is the URL that lets us know where to send the Sentry error reports"
echo "You can find in your project settings on sentry.io under 'Client Keys (DSN)'"
echo ""
echo "Leave blank to disable Sentry"
echo ""


if [[ -z "$CURRENT_VALUE" ]]
then
    read -r -p "Supplier Stock Watcher DSN: " SSW_SENTRY_DSN
else
    read -r -e -p "Supplier Stock Watcher DSN: " -i "$CURRENT_VALUE" SSW_SENTRY_DSN
fi

SSW_SENTRY_DSN=$(echo "$SSW_SENTRY_DSN" | tr -d ' ' | tr -d '\n')

if [[ -z "$SSW_SENTRY_DSN" ]]
then
    echo "Sentry disabled for Supplier Stock Watcher"
else
    if grep -q "SSW_SENTRY_DSN=" .env
    then
        # Update existing entry
        # Source: https://stackoverflow.com/a/2705678/6423456
        ESCAPED_SSW_SENTRY_DSN=$(printf '%s\n' "$SSW_SENTRY_DSN" | sed -e 's/[\/&]/\\&/g')
        sed -i "s/^SSW_SENTRY_DSN=.*/SSW_SENTRY_DSN=${ESCAPED_SSW_SENTRY_DSN}/g" .env
    else
        # Add new entry
        echo "SSW_SENTRY_DSN=$SSW_SENTRY_DSN" >> .env
    fi
    echo -e "\nSupplier Stock Watcher Sentry DSN configured"
fi

##############
# Secret Key #
##############

clear

# Generate secret key

CURRENT_VALUE=$(grep "SECRET_KEY=" .env | cut -d '=' -f 2)

if [[ -z "$CURRENT_VALUE" ]]
then
    echo "Generating random secret key..."

    # Generate random key
    # Exclude single and double quotes so we don't run into problems with
    # Python strings with unescaped quotes
    SECRET_KEY=$(cat /dev/urandom | tr -dc '[:print:]' | tr '"' ';' | tr "'" ';' | fold -w "${1:-50}" | head -1)

    # Add new entry
    echo "SECRET_KEY='$SECRET_KEY'" >> .env

    echo "Random secret key generated"
fi

#############
# Redis Key #
#############

# Generate redis key

CURRENT_VALUE=$(grep "REDIS_KEY=" .env | cut -d '=' -f 2)

if [[ -z "$CURRENT_VALUE" ]]
then
    echo "Generating random redis key..."

    # Generate random key
    # Exclude all non-alpha-numeric characters
    # Python strings with unescaped quotes
    REDIS_KEY=$(cat /dev/urandom | tr -dc '[:alnum:]' | fold -w "${1:-50}" | head -1)

    # Add new entry
    echo "REDIS_KEY='$REDIS_KEY'" >> .env

    echo "Random redis key generated"
fi
